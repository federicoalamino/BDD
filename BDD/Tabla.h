//
// Created by falamino on 30/10/19.
//

#ifndef TP3_TABLA_H
#define TP3_TABLA_H

#include <set>
#include "Registro.h"
#include "modulos_basicos/linear_set.h"
#include "modulos_basicos/string_map.h"


using namespace std;
class Tabla {
public:
    Tabla(vector<NombreCampo> &nc, NombreCampo &clave);
    vector<NombreCampo> Campos();
    vector<Registro> registros();
    NombreCampo clave();
    size_t cantRegistros();
    vector<Registro> valorDeCampoIgual(NombreCampo nc1, NombreCampo nc2);
    Registro obtenerPorValor(Valor v);
    bool estaDefinido(Valor v);
    vector<Registro> agregarRegParaSelect(NombreCampo nc, Valor v);
    vector<pair<Registro,Valor> > coincidenciaPorClave(vector<Valor> cv);
    vector<Valor> clavesDelTrie();
    void insertar(Registro r);
    void borrarDeTabla(Valor v);

private:


    vector<NombreCampo> _nombreCampos;
    string_map<Registro> _valoresDeClave;
    vector<Registro> _registros;
    NombreCampo _clave;
    //friend class BDD;
};

#endif //TP3_TABLA_H
