#include "Registro.h"
using namespace std;
//#include "modulos_basicos/linear_set.h"
Registro::Registro() {
    this->_size =0;
}

Registro::Registro(set<NombreCampo> nc) {
    this->_arregloCampoValor = vector<campoValor>(nc.size());
    this->_size = nc.size();
    int i = 0;
    while(i<this->_size){
        set<NombreCampo>::iterator provisional = nc.begin();
        this->_arregloCampoValor[i].first = *provisional;
        nc.erase(*provisional);
        i++;
    }
}


set<NombreCampo> Registro::campos() const {
    set<NombreCampo> res;
    for(int i=0; i<this->_size; i++){
        res.insert(_arregloCampoValor[i].first);
    }
    return res;
}

Valor& Registro::operator[](const NombreCampo& campo) {
    int res = -1;
    for(int i = 0; i < this->_size; i++){
      if(campo == this->_arregloCampoValor[i].first){
          res = i;
      }
    }
    return this->_arregloCampoValor[res].second;
}

void Registro::definirCampo(NombreCampo nc, Valor v){
    if(CampoDefinidoEnElArray(nc)) {
        int pos = Registro::posicionDeCampoEnElArray(nc);
        this->_arregloCampoValor[pos].first = nc;
        this->_arregloCampoValor[pos].second = v;
    } else{
        campoValor temp;
        temp.first = nc;
        temp.second = v;
        _arregloCampoValor.push_back(temp);
        _size++;
    }
}

bool Registro::CampoDefinidoEnElArray(NombreCampo nc){
  bool res = false;
  int prov = this->_size;
  for(int i = 0; i < prov;i++){
      if(nc == this->_arregloCampoValor[i].first){
          res=true;
      }
  }
    return res;
}

int Registro::posicionDeCampoEnElArray(NombreCampo nc) const{
    int res = 0;
    int i = 0;
    while(i < this->_size ){
       if(nc == this->_arregloCampoValor[i].first)
           i = this->_size+1;
       else{
           res++;
       }
       i++;
    }
    return res;
}

NombreCampo Registro::campo(int i) {
    return this->_arregloCampoValor[i].first;
}


Registro Registro::concat(const Registro &r) const{
    Registro res;
    for(int i = 0; i < this->_size; i++) {
        res.definirCampo(this->_arregloCampoValor[i].first,_arregloCampoValor[i].second);
    }
    for(int i = 0; i< r.tamanio(); i++){
        res.definirCampo(r.actual(i).first, r.actual(i).second);
    }
    return res;
}

Registro Registro::cambiar(NombreCampo nc1, NombreCampo nc2){
    Registro res = Registro(this->campos()); //Si bien el constructor los pasa "random" al vector a los campos. Los piso, asique no hay problema.
    int i = 0;
    int pos = this->posicionDeCampoEnElArray(nc1);
    while(i<this->_size){
        if(i == pos){
            res.definirCampo(nc2,this->_arregloCampoValor[i].second);
            i++;
        }
        else{
            res.definirCampo(this->_arregloCampoValor[i].first,this->_arregloCampoValor[i].second);
            i++;
        }
    }
    return res;
}

bool Registro::operator==(Registro &r){
    bool res = true;
    if(this->_size != r.tamanio()){
        res = false;
    }
    else{
        int i = 0;
        while(i< _size){
            if(this->_arregloCampoValor[i]!= r.actual(i)){
                res = false;
            }
            i++;
        }
    }
    return res;
}

campoValor Registro::actual(int i) const{
    campoValor res;
    res.first = _arregloCampoValor[i].first;
    res.second = _arregloCampoValor[i].second;
    return res;
}



bool Registro::operator==(const Registro &r) const{
    bool res = true;
    if(this->_size != r.tamanio()){
        res = false;
    } else{
        int i = 0;
        while(i< _size){
            if(this->_arregloCampoValor[i]!= r.actual(i)){
                res = false;
            }
            i++;
        }
    }
    return res;
}

Valor Registro::value(int i) {
return this->_arregloCampoValor[i].second;
}

bool Registro::compare(Registro &r){
    bool res = true;
    int i = 0;
    while(i<this->_size){
        if(this->_arregloCampoValor[i].second != r.value(i)){
            res = false;
        }
    }
    return res;
}

int Registro::tamanio() const{
    return this->_size;
}
