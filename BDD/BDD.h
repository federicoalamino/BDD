#ifndef TP3_BDD_H
#define TP3_BDD_H

#include <set>
#include "Tabla.h"
#include "Tipos.h"
#include "Registro.h"
#include "Consulta.h"
#include "modulos_basicos/linear_set.h"
#include "modulos_basicos/string_map.h"


using namespace std;

class BDD {
public:
    BDD();
    vector<NombreTabla> Tablas();
    vector<Valor> valoresCampoClave(NombreTabla nt);
    Tabla Table(NombreTabla nt);
    vector<Registro> hacerSelect(const Consulta& c);
    vector<Registro> selectConYSinClave(NombreTabla nt,NombreCampo nc, Valor v);
    vector<Registro> selectNormal(const vector<Registro> &cr, NombreCampo nc, Valor v);
    vector<Registro> dobleSelect(const Consulta& c);
    vector<Registro> join(NombreTabla nt1, NombreTabla nt2);
    vector<Registro> cltaUnion(const vector<Registro> &cr1,const vector<Registro> &cr2);    //union es un metodo de c por eso hicimos el renombre de csltUnion
    vector<Registro> hacerMatch(const Consulta& c);
    vector<Registro> match(const vector<Registro> &cr,NombreCampo nc1, NombreCampo nc2);
    vector<Registro> product(const vector<Registro> &cr1,const vector<Registro> &cr2);
    vector<Registro> rename(const vector<Registro> &cr,NombreCampo nc1, NombreCampo nc2);
    vector<Registro> hacerInter(const Consulta& c);
    vector<Registro> inter(const vector<Registro> &cr1,const vector<Registro> &cr2);
    vector<Registro> selectProduct(NombreTabla nt1,NombreTabla nt2,NombreCampo nc,Valor v);
    vector<Registro> interFromFrom(NombreTabla nt1,NombreTabla nt2);
    vector<Registro> proj(set<NombreCampo> cnc,const vector<Registro> &cr);
    NombreTabla min(NombreTabla nt1, NombreTabla nt2);
    NombreTabla max(NombreTabla nt1, NombreTabla nt2);
    vector<Registro> consultar(const Consulta &c);
    void agregarTabla(Tabla t, NombreTabla nt);
    void eliminarTabla(NombreTabla nt);
    void agregarRegistro(NombreTabla &nt, Registro &r);
    void eliminarRegistro(NombreTabla nt, Valor &v);
    friend class Consulta;
private:
    //////////Funciones privadas////////////////////

    //////////////////////////////////////////////////////

    string_map<Tabla> _tablas;


    // COMPLETAR con la representación privada.
};




#endif //TP3_BDD_H
