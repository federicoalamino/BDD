#ifndef __REGISTRO_H__
#define __REGISTRO_H__

#include <set>
#include "modulos_basicos/linear_set.h"
#include "Tipos.h"
#include <vector>

typedef pair<NombreCampo,Valor> campoValor;

using namespace std;

class Registro {
public:
    Registro();
    Registro(set<NombreCampo> nc);
    set<NombreCampo> campos() const;
    Valor& operator[](const NombreCampo& campo);
    Valor value(int i);     //Dado un int en el arreglo me devuelve el valor correspondiente
    NombreCampo campo(int i); //Idem arriba pero el NombreCampo
    bool CampoDefinidoEnElArray(NombreCampo nc);
    int posicionDeCampoEnElArray(NombreCampo nc) const;
    Registro concat(const Registro &r) const;            //Concatena 2 registros.
    Registro cambiar(NombreCampo nc1, NombreCampo nc2);  //Cambia nombre campo 1 por nombre campo 2
    bool operator==(Registro &r); // 2 reg =
    bool operator==(const Registro &r) const;
    bool compare(Registro &r); // 2 reg = (precondicion mas fuerte que arriba - complej)
    pair<NombreCampo,Valor> actual(int i) const; //me devuelve el elemento ubicado en la pos i del vector
    int tamanio() const;
    void definirCampo(NombreCampo nc, Valor v); //Dado un campo lo define con un valor

private:
    //Funciones privadas ///
    ///////////////////////
    vector<campoValor> _arregloCampoValor;
    int _size;
    //friend class BDD;
};
//Agregar un metodo que dado una pos me devuelva la tupla

#endif /*__REGISTRO_H__*/
