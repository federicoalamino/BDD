
#include <unordered_set>
#include "Tabla.h"
#include "Registro.h"

using namespace std;

Tabla::Tabla(vector<NombreCampo> &nc, NombreCampo &clave): _nombreCampos(nc) {
    _registros ={};
    string_map<Registro> valoresDeClave = string_map<Registro>();
    _valoresDeClave = valoresDeClave;
    _clave = clave;
}

void Tabla::insertar(Registro r) {
    this->_registros.push_back(r);
    Valor res = r.value(r.posicionDeCampoEnElArray(this->_clave));  // Obtengo el valor del campo  de "r" que es el mismo que la clave de "t"
    this->_valoresDeClave.operator[](res) = r; //Defino el valor res con el significado r
}


void Tabla::borrarDeTabla(Valor v) {
    this->_valoresDeClave.erase(v);
}

vector<NombreCampo> Tabla::Campos() {
    return _nombreCampos;
}

vector<Registro> Tabla::registros(){
    return this->_registros;
}

NombreCampo Tabla::clave() {
    return this->_clave;
}

size_t Tabla::cantRegistros() {
    return this->_registros.size();
}

vector<Registro> Tabla::valorDeCampoIgual(NombreCampo nc1, NombreCampo nc2) {
    vector<Registro> res;
    vector<string> claves = this->_valoresDeClave.claves();
    auto it = claves.begin();
    Registro provisorio = _valoresDeClave.at(*it);
    int cont1 = provisorio.posicionDeCampoEnElArray(nc1);
    int cont2 = provisorio.posicionDeCampoEnElArray(nc2);
    while(it != claves.end()){
        Registro actual = _valoresDeClave.at(*it);
        if(actual.value(cont1) == actual.value(cont2)){
            res.push_back(actual);
        }
        it.operator++();
    }
    return res;
}

Registro Tabla::obtenerPorValor(Valor v) {
    return this->_valoresDeClave.at(v);
}

bool Tabla::estaDefinido(Valor v) {
    int res = this->_valoresDeClave.count(v);
    return res == 1;
}

vector<Registro> Tabla::agregarRegParaSelect(NombreCampo nc, Valor v) { //REVISAR!!!!!!!!
    vector<Registro> res;
    vector<Valor> ConjClaves = this->_valoresDeClave.claves();
    auto ite = ConjClaves.begin();
    Registro provisorio = this->_valoresDeClave.at(*ite);
    int cont = provisorio.posicionDeCampoEnElArray(nc);
    while(ite != ConjClaves.end()){
        Registro RegistroDeITE = this->_valoresDeClave.at(*ite);
        if(RegistroDeITE.value(cont) == v ) res.push_back(RegistroDeITE);
        ite.operator++();
    }
    return res;
}

vector<pair<Registro,Valor> > Tabla::coincidenciaPorClave(vector<Valor> cv) {
    vector<pair<Registro,Valor>> res;
    uint i = 0;
    while(i < cv.size()){
        Valor prov = cv[i];
        if(this->estaDefinido(prov)){   //Si esta definido el valor entonces
            pair<Registro,Valor> provisional  = make_pair(this->_valoresDeClave.at(prov),prov);
            res.push_back(provisional);
        }
        i++;     // avanzo al siguiente elemento
    }
    return res;
}

vector<Valor> Tabla::clavesDelTrie() {
    return this->_valoresDeClave.claves();
}
