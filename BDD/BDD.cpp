#include "BDD.h"


BDD::BDD() : _tablas() {
}

void BDD::agregarTabla(Tabla t, NombreTabla nt) {
 this->_tablas.insert(make_pair(nt, t));
}

vector<NombreTabla> BDD::Tablas() {
 return this->_tablas.claves();
}

void BDD::eliminarTabla(NombreTabla nt) {
 this->_tablas.erase(nt);
}

void BDD::eliminarRegistro(NombreTabla nt, Valor& v){
    this->Table(nt).borrarDeTabla(v);
}

void BDD::agregarRegistro(NombreTabla& nt, Registro &r) {
    (this-> _tablas.at(nt)).insertar(r);
}

vector<Valor> BDD::valoresCampoClave(NombreTabla nt) {
return this->Table(nt).clavesDelTrie();
}

Tabla BDD::Table(NombreTabla nt) {
        return this-> _tablas.at(nt);
}

vector<Registro> BDD::hacerSelect(const Consulta& c) {
    vector<Registro> res;
    if (c.subconsulta1().tipo_consulta() == FROM) {
        res = this->selectConYSinClave(c.subconsulta1().nombre_tabla(), c.campo1(), c.valor());
    } else if (c.subconsulta1().tipo_consulta() == SELECT &&  c.subconsulta1().subconsulta1().tipo_consulta() == FROM &&c.subconsulta1().campo1() == this->_tablas.at(c.subconsulta1().subconsulta1().nombre_tabla()).clave()) {
        res = this->dobleSelect(c);
    } else if (c.subconsulta1().tipo_consulta() == PRODUCT && c.subconsulta1().subconsulta1().tipo_consulta() == FROM &&
               c.subconsulta1().subconsulta2().tipo_consulta() == FROM) {
        res = this->selectProduct(c.subconsulta1().subconsulta1().nombre_tabla(),
                            c.subconsulta1().subconsulta2().nombre_tabla(), c.campo1(), c.valor());
    } else{
        vector<Registro> prov = this->consultar(c.subconsulta1());
        res = this->selectNormal(prov,c.campo1(),c.valor());
    }
    return res;
}

vector<Registro> BDD::selectConYSinClave(NombreTabla nt, NombreCampo nc, Valor v) {
    vector<Registro> res;
    if (this->_tablas.at(nt).clave() == nc){
        if(this->_tablas.at(nt).estaDefinido(v)){
            res.push_back(this->_tablas.at(nt).obtenerPorValor(v));
        }
    } else{
        res = this->_tablas.at(nt).agregarRegParaSelect(nc,v);
    }
    return res;
}


vector<Registro> BDD::selectNormal(const vector<Registro> &cr, NombreCampo nc, Valor v) {
    vector<Registro> res;
    uint i = 0;
    int pos = cr[i].posicionDeCampoEnElArray(nc);
    while(i < cr.size()){
        if(cr[i].actual(pos).second == v){
            res.push_back(cr[i]);
        }
        i++;
    }
    return res;
}

vector<Registro> BDD::dobleSelect(const Consulta& c) { //REVISAR TANTO ESTE ALGORITMO COMO EL DEL TP2 PARA VER SI HACE LO QUE DEBERIA HACER
    vector<Registro> res;
    vector<Registro> prov;
    prov = this->selectConYSinClave(c.subconsulta1().subconsulta1().nombre_tabla(),c.campo1(),c.valor());
    int i = 0;
    int pos = prov[i].posicionDeCampoEnElArray(c.subconsulta1().campo1());
    if(prov[i].value(pos) == c.subconsulta1().valor()){
        res.push_back(prov[i]);
    }
    return res;
}

vector<Registro> BDD::join(NombreTabla nt1, NombreTabla nt2) {
    vector<Registro> res;
    NombreTabla nt3 = min(nt1,nt2);
    NombreTabla nt4 = max(nt1,nt2);
    Tabla t3 = this->_tablas.at(nt3);
    Tabla t4 = this->_tablas.at(nt4);
    vector<pair<Registro,Valor> > prov = t4.coincidenciaPorClave(t4.clavesDelTrie());
    for(auto it = prov.begin();it != prov.end(); it.operator++()){
        Registro r = (*it).first;
        Valor v = (*it).second;
        res.push_back(r.concat(t3.obtenerPorValor(v)));
    }
    return res;
}

vector<Registro> BDD::cltaUnion(const vector<Registro> &cr1,const vector<Registro> &cr2) {
    vector<Registro> res;
    for(auto it = cr1.begin();it != cr1.end();it.operator++()){
        res.push_back((*it));
    }
    for(auto it = cr2.begin();it != cr2.end();it.operator++()){
        res.push_back((*it));
    }
    return res;
}

vector<Registro> BDD::hacerMatch(const Consulta& c) {
    vector<Registro> res;
    if(c.subconsulta1().tipo_consulta() == PRODUCT && c.subconsulta1().subconsulta1().tipo_consulta() == FROM && c.subconsulta1().subconsulta2().tipo_consulta() == FROM){
        res = join(c.subconsulta1().subconsulta1().nombre_tabla(),c.subconsulta1().subconsulta2().nombre_tabla());
    } else{
        res = match(consultar(c.subconsulta1()),c.campo1(),c.campo2());
    }
    return res;
}


vector<Registro> BDD::match(const vector<Registro> &cr,NombreCampo nc1, NombreCampo nc2) {
    vector<Registro> res;
    Registro prov = cr[0];
        int pos1 = prov.posicionDeCampoEnElArray(nc1);
        int pos2 = prov.posicionDeCampoEnElArray(nc2);
        for (uint i = 0; i < cr.size(); i++) {
            if (cr[i].actual(pos1).second == cr[i].actual(pos2).second) {
                res.push_back(cr[i]);
            }
        }
        return res;

}


vector<Registro> BDD::product(const vector<Registro> &cr1,const vector<Registro> &cr2) {
    vector<Registro> res;
    for(uint i = 0; i<cr1.size(); i++){
        for(uint j = 0; j<cr2.size(); j++){
            Registro prov;
            prov = cr1[i].concat(cr2[j]);
            res.push_back(prov);
        }
    }
    return res;
}





vector<Registro> BDD::rename(const vector<Registro> &cr, NombreCampo nc1, NombreCampo nc2) {
    vector<Registro> res;
    auto it = cr.begin();
    while(it != cr.end()){
        Registro r = *it;
        res.push_back(r.cambiar(nc1,nc2));
        it.operator++();
    }
    return res;
}

vector<Registro> BDD::hacerInter(const Consulta& c) {
    vector<Registro> res;
    if(c.subconsulta1().tipo_consulta() == FROM && c.subconsulta2().tipo_consulta() == FROM){
        res = interFromFrom(c.subconsulta1().nombre_tabla(),c.subconsulta2().nombre_tabla());
    } else{
        res = inter(consultar(c.subconsulta1()),consultar(c.subconsulta2()));
    }
    return res;
}

vector<Registro> BDD::inter(const vector<Registro> &cr1,const vector<Registro> &cr2) {
    vector<Registro> res;
    for(uint i = 0; i<cr1.size(); i++){
        for(uint j = 0; j<cr2.size(); j++){
            if(cr1[i]==cr2[j]) {
                res.push_back(cr1[i]);
                break;
            }
        }
    }

    return res;
}

vector<Registro> BDD::selectProduct(NombreTabla nt1, NombreTabla nt2, NombreCampo nc, Valor v) {
    vector<Registro> res;
    Tabla t1 = this->_tablas.at(nt1);
    Tabla t2 = this->_tablas.at(nt2);
    if(t1.estaDefinido(v)){
        Registro prov = t1.obtenerPorValor(v);
        vector<Registro> prov2 = t2.registros();
        for(auto it = prov2.begin();it != prov2.end();it.operator++()){
            res.push_back(prov.concat(*it));
        }
    }
    return res;
}


vector<Registro> BDD::interFromFrom(NombreTabla nt1, NombreTabla nt2) { //REVISAR funcionamiento
    vector<Registro> res;
    NombreTabla nt3 = min(nt1,nt2);
    NombreTabla nt4 = max(nt1,nt2);
    Tabla t3 = this->_tablas.at(nt3);
    Tabla t4 = this->_tablas.at(nt4);
    vector<Registro> prov = t3.registros();
    int pos = prov[0].posicionDeCampoEnElArray(t3.clave());
    for(uint i = 0; i<prov.size(); i++){
        if(t4.estaDefinido(prov[i].value(pos))){
            if(prov[i]==t4.obtenerPorValor(prov[i].value(pos))){
                res.push_back(prov[i]);
            }
        }

    }
    return res;
}


vector<Registro> BDD::proj(set<NombreCampo> cnc,const vector<Registro> &cr) {
    vector<Registro> res;
    auto it = cr.begin();
    while(it != cr.end()){
        Registro prov = Registro(cnc);
        Registro r = *it;
        for(auto it2 = cnc.begin();it2 != cnc.end(); it2.operator++()){
            NombreCampo c = *it2;
            int pos1 = r.posicionDeCampoEnElArray(c);
            prov.definirCampo(c,r.value(pos1));
        }
        res.push_back(prov);
        it.operator++();
    }
    return res;
}

NombreTabla BDD::min(NombreTabla nt1, NombreTabla nt2) {
 NombreTabla res = nt2;
 if(this->Table(nt1).clavesDelTrie().size() <= this->Table(nt2).clavesDelTrie().size()){
     res = nt1;
 }
 return res;
}

NombreTabla BDD::max(NombreTabla nt1, NombreTabla nt2) {
 NombreTabla res = nt2;
 if(this->Table(nt1).clavesDelTrie().size() > this->Table(nt2).clavesDelTrie().size()) {
     res = nt1;
 }
 return res;
}


vector<Registro> BDD::consultar(const Consulta &c) {
    vector<Registro> res;
    switch (c.tipo_consulta()){
        case FROM:
            res = this-> Table(c.nombre_tabla()).registros();
            break;
        case SELECT:
            res = hacerSelect(c);
            break;
        case MATCH:
            res = hacerMatch(c);
            break;
        case PROJ:
            res = proj(c.conj_campos(),consultar(c.subconsulta1()));
            break;
        case RENAME:
            res = rename(consultar(c.subconsulta1()),c.campo1(),c.campo2());
            break;
        case INTER:
            res = hacerInter(c);
            break;
        case UNION:
            res = cltaUnion(consultar(c.subconsulta1()),consultar(c.subconsulta2()));
            break;
        case PRODUCT:
            res = product(consultar(c.subconsulta1()),consultar(c.subconsulta2()));
            break;
    }
    return res;
}
